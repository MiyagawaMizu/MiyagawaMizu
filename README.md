<a href="https://discord.com/users/738748102311280681"><img align="right" alt="profile" width=50% src="https://lanyard.kyrie25.me/api/738748102311280681?imgStyle=square&gradient=e9d6d5-e9d6d5-f3b1b4-ffffff&bg=0d1117"></a>


### Hey, I'm Mizu 🌸

I'm a developer and gamer.

## ☕ About me

I'm Hikikomori plus NEET and my hobbies are playing games, watching anime, reading manga, and sometimes tinkering with random stuff.
## 💻 Experience

<img src="https://cdn.discordapp.com/emojis/1059638606941130872.webp?size=160&quality=lossless" width="32"></img> Still learning, I don't have anything special yet. But I hope to be able to change that in the future.
<br>

## 📊 Stats

<div><img width="50%" src="https://github-readme-stats.vercel.app/api?username=MiyagawaMizu&show_icons=true&count_private=true&theme=react&hide_border=true&bg_color=0D1117"/> <img width="45%" src="https://github-readme-stats.vercel.app/api/top-langs/?username=MiyagawaMizu&show_icons=true&count_private=true&theme=react&hide_border=true&bg_color=0D1117&layout=compact"/></div>

 

## 📫 Contact
Please Contact me on Discord for a prompt response: [Mizu#2203](https://discord.com/users/738748102311280681).

You can also email me here: miyagawamizu@courvix.com

<a href="https://github.com/Meghna-DAS/github-profile-views-counter"><img src="https://komarev.com/ghpvc/?username=MiyagawaMizu"></a><!-- <a href="https://github.com/MiyagawaMizu"><img src="https://img.shields.io/github/followers/MiyagawaMizu?label=Followers&style=social" alt="GitHub Badge"></a> -->
<a href="https://discord.com/users/350945523810959361"><img alt="Discord" src="https://img.shields.io/badge/Discord-7289DA?logo=discord&logoColor=white"/> <a href="https://www.facebook.com/miyagawamizu"><img src="https://img.shields.io/badge/Facebook-1877F2?logo=facebook&logoColor=white"/></a> </a><a href="mailto:miyagawamizu@courvix.com"><img src="https://img.shields.io/badge/Mail-D14836?logo=gmail&logoColor=white"/></a> <a href="https://ko-fi.com/miyagawamizu"><img src="https://img.shields.io/badge/Kofi-ff5c5a?logo=ko-fi&logoColor=white"/></a>
